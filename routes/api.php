<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function () {

    Route::post('main/import', [App\Http\Controllers\MainController::class, 'import'])->name('main.import');

    Route::post('main/data', [App\Http\Controllers\MainController::class, 'data'])->name('main.data');

    Route::post('main/detail', [App\Http\Controllers\MainController::class, 'detail'])->name('main.detail');

});