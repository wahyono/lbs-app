<!-- jQuery -->
<script src="{{ URL::asset('assets/adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ URL::asset('assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('assets/adminlte/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ URL::asset('assets/adminlte/dist/js/demo.js') }}"></script>

<script src="{{ URL::asset('assets/adminlte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>


<script src="{{ URL::asset('assets/adminlte/plugins/icheck/icheck.min.js') }}" type="text/javascript"></script>