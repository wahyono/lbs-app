<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-12">
					<h1>@yield('content_title')</h1>
				</div>
			</div>
		</div>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="container-fluid">
			@yield('contents')
		</div>
		<!-- /.card -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->