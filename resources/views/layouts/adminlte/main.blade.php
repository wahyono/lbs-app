
<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts/adminlte/includes/head')

    @yield('styles')
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    @include('layouts/adminlte/includes/navbar')

    @include('layouts/adminlte/includes/sidebar')

    @include('layouts/adminlte/includes/content')

    @include('layouts/adminlte/includes/footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

    @include('layouts/adminlte/includes/script')

    @yield('scripts')
</body>
</html>
