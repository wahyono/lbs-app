<!DOCTYPE html>
<html lang="en">

    @include('layouts/adminkit/parts/head')

    <body>
        <div class="wrapper">
            @include('layouts/adminkit/parts/sidebar')

            <div class="main">

                @include('layouts/adminkit/parts/navbar')

                <main class="content">
                    <div class="container-fluid p-0">

                        @yield('content')

                    </div>
                </main>

                @include('layouts/adminkit/parts/footer')

            </div>
        </div>

        @include('layouts/adminkit/parts/scripts')

    </body>

</html>