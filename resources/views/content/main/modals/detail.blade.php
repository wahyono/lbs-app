<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form role="form" id="formAdd">
                <div class="modal-header">
                    <h5 class="modal-title">Detail Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="margin-bottom: 15px;">
                                <table class="">
                                    <tr>
                                        <td>Subjek</td>
                                        <td>:</td>
                                        <td id="detail_subjek"></td>
                                    </tr>
                                    <tr>
                                        <td>Waktu</td>
                                        <td>:</td>
                                        <td id="detail_waktu"></td>
                                    </tr>
                                </table>
                            </div>
                            <div style="margin-bottom: 15px;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>LBS Lat</th>
                                            <th>LBS Lng</th>
                                            <th>WA Lat</th>
                                            <th>WA Lng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="detail_lbs_lat"></td>
                                            <td id="detail_lbs_lng"></td>
                                            <td id="detail_wa_lat"></td>
                                            <td id="detail_wa_lng"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="margin-bottom: 15px;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>IT</th>
                                            <th>SV</th>
                                            <th>Medsos</th>
                                            <th>Pendanaan</th>
                                            <th>Agent Handling</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="detail_info_it"></td>
                                            <td id="detail_info_sv"></td>
                                            <td id="detail_info_medsos"></td>
                                            <td id="detail_info_dana"></td>
                                            <td id="detail_info_agen_handling"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="margin-bottom: 15px;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Intel Tek</th>
                                            <th>Niat</th>
                                            <th>Motivasi</th>
                                            <th>Kelompok</th>
                                            <th>Akses</th>
                                            <th>Dana</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="detail_analisa_intel_tek"></td>
                                            <td id="detail_analisa_niat"></td>
                                            <td id="detail_analisa_motivasi"></td>
                                            <td id="detail_analisa_kelompok"></td>
                                            <td id="detail_analisa_akses"></td>
                                            <td id="detail_analisa_dana"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="margin-bottom: 15px;">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Low</th>
                                            <th>Medium</th>
                                            <th>High</th>
                                            <th>Middle</th>
                                            <th>Netral</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="detail_ancaman_low"></td>
                                            <td id="detail_ancaman_medium"></td>
                                            <td id="detail_ancaman_high"></td>
                                            <td id="detail_ancaman_middle"></td>
                                            <td id="detail_ancaman_netral"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Tindakan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td id="detail_tindakan"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>