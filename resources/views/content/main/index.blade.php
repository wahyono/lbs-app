@extends('layouts.adminlte.main')

@section('contents')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Data</div>
                <div class="card-tools">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#modalAdd">
                        <i class="fa fa-plus" style="margin-right: 5px;"></i> Add
                    </button>
                </div>
            </div>
            <div class="card-body">
                <table id="table-order" class="table table-hover">
                    <thead>
                        <tr>
                            <th style="vertical-align: middle !important;">
                                <div class="icheck-list">
                                    <label for="">
                                        <input type="checkbox" id="select-check-main" class="" data-ref="1">
                                    </label>
                                </div>
                            </th>
                            <th style="vertical-align: middle !important;">Subyek</th>
                            <th style="vertical-align: middle !important;">Waktu</th>
                            <th style="vertical-align: middle !important;">LBS Lat</th>
                            <th style="vertical-align: middle !important;">LBS Lng</th>
                            <th style="vertical-align: middle !important;">WA Lat</th>
                            <th style="vertical-align: middle !important;">WA Lng</th>
                            <th style="vertical-align: middle !important;">Tindakan</th>
                            <th class="sticky-col" style="vertical-align: middle !important;">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="card-footer clearfix">
                <div id="page-info" class="float-left"></div>
                <div id="pagination" class="float-right"></div>
            </div>
        </div>
    </div>
</div>

@include('content/main/modals/add')
@include('content/main/modals/detail')

@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#select-check-main').iCheck({
            checkboxClass: 'icheckbox_flat-blue'
        });

        $('#select-check-main').on('ifChecked', function() {
            $('.select-check').each(function() {
                $(this).iCheck('check');
            });
        });

        $('#select-check-main').on('ifUnchecked', function() {
            $('.select-check').each(function() {
                $(this).iCheck('uncheck');
            });
        });

        $('body').on('click', '.paginate', function() {
            getData($(this).attr('data-page'));
        });

        $('body').on('ifChecked', '.select-multiple', function() {
            var status = false;
            var refs = [];
            $('.select-multiple').each(function() {
                if ($(this).is(':checked')) {
                    refs.push($(this).attr('data-ref'));
                } else {
                    var index = refs.indexOf($(this).attr('data-ref'));

                    if (index != -1) {
                        refs.splice(index, 1);
                    }
                }
            });

            if (refs.length > 0) {
                status = true;
            }

            if (status) {
                $('#select-multiple-info').css('display', 'block');
                $('#select-multiple-info-records').html('Selected ' + refs.length + ' records:');
            } else {
                $('#select-multiple-info').css('display', 'none');
                $('#select-multiple-info-records').html('');
            }
        });

        $('body').on('ifUnchecked', '.select-multiple', function() {
            var status = false;
            var refs = [];
            $('.select-multiple').each(function() {
                if ($(this).is(':checked')) {
                    refs.push($(this).attr('data-ref'));
                } else {
                    var index = refs.indexOf($(this).attr('data-ref'));

                    if (index != -1) {
                        refs.splice(index, 1);
                    }
                }
            });

            if (refs.length > 0) {
                status = true;
            }

            if (status) {
                $('#select-multiple-info').css('display', 'block');
                $('#select-multiple-info-records').html('Selected ' + refs.length + ' records:');
            } else {
                $('#select-multiple-info').css('display', 'none');
                $('#select-multiple-info-records').html('');
                $('#select-check-main').iCheck('uncheck');
            }
        });

        $('#formAdd').on('submit', function(e){
            e.preventDefault();

            var formData = new FormData();
            formData.append('api_token', '{{ Auth::user()->api_token }}');
            formData.append('file', $('input[name="file"]')[0].files[0]);
            // var formInputs = $(this).serializeArray();

            // for(var i = 0; i < formInputs.length; i++) {
            //     formData.append(formInputs[i]['name'], formInputs[i]['value']);
            // }

            $.ajax({
                url: '{{ route("main.import") }}',
                type: 'post',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    Swal.fire({
                        title: 'Please wait...',
                        text: 'Just wait!',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        showDenyButton: false,
                        showCancelButton: false,
                        showConfirmButton: false
                    });
                },
                success: function(response) {
                    var title;

                    if (response['status']) {
                        title = 'Berhasil mengupdate data';
                    } else {
                        title = 'Gagal mengupdate data';
                    }

                    Swal.fire({
                        title: title,
                        showDenyButton: false,
                        showCancelButton: false,
                        confirmButtonText: 'Ok',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then((result) => {
                        getData();
                        $('#modalAdd').modal('hide');
                    });
                },
                error: function(request, status, error) {
                    Swal.fire({
                        title: 'Gagal mengupdate data. Terjadi kesalahan!',
                        showDenyButton: false,
                        showCancelButton: false,
                        confirmButtonText: 'Ok',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    });
                }
            });
        });

        $('body').on('click', '.detail-data', function(){
            getDetail($(this).attr('data-ref'));
        });

        function getData(page = 1) {
            $.ajax({
                url: '{{ route("main.data") }}',
                type: 'post',
                data: {
                    page: page,
                    paginate: 10,
                    api_token: '{{ Auth::user()->api_token }}'
                },
                beforeSend: function() {
                    $('#table-order tbody').html('');
                    $('#pagination').html('');

                    Swal.fire({
                        title: 'Please wait',
                        text: 'Just wait, please...',
                        showDenyButton: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    });
                },
                success: function(response) {
                    generateData(response['data'], response['start_page']);
                    pagination(response['options'], response['start_page'], response['paginate']);
                    Swal.close();
                },
                error: function(request, status, error) {
                    Swal.fire('Failed to load data. Error occured!', '', 'error');
                }
            });
        }

        function getDetail(id) {
            $.ajax({
                url: '{{ route("main.detail") }}',
                type: 'post',
                data: {
                    id: id,
                    api_token: '{{ Auth::user()->api_token }}'
                },
                beforeSend: function() {
                    $('#detail*').html('');

                    Swal.fire({
                        title: 'Please wait',
                        text: 'Just wait, please...',
                        showDenyButton: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    });
                },
                success: function(response) {
                    $('#detail_subjek').html(response.subyek);
                    $('#detail_waktu').html(response.waktu);
                    $('#detail_lbs_lat').html(response.lbs_lat);
                    $('#detail_lbs_lng').html(response.lbs_lng);
                    $('#detail_wa_lat').html(response.wa_lat);
                    $('#detail_wa_lng').html(response.wa_lng);
                    $('#detail_info_it').html(response.info_it);
                    $('#detail_info_sv').html(response.info_sv);
                    $('#detail_info_medsos').html(response.info_medsos);
                    $('#detail_info_dana').html(response.info_dana);
                    $('#detail_info_agen_handling').html(response.info_agen_handling);
                    $('#detail_analisa_intel_tek').html(response.analisa_intel_tek);
                    $('#detail_analisa_niat').html(response.analisa_niat);
                    $('#detail_analisa_motivasi').html(response.analisa_motivasi);
                    $('#detail_analisa_kelompok').html(response.analisa_kelompok);
                    $('#detail_analisa_akses').html(response.analisa_akses);
                    $('#detail_analisa_dana').html(response.analisa_dana);
                    $('#detail_ancaman_low').html(response.ancaman_low);
                    $('#detail_ancaman_medium').html(response.ancaman_medium);
                    $('#detail_ancaman_high').html(response.ancaman_high);
                    $('#detail_ancaman_middle').html(response.ancaman_middle);
                    $('#detail_ancaman_netral').html(response.ancaman_netral);
                    $('#detail_tindakan').html(response.tindakan);
                    Swal.close();
                },
                error: function(request, status, error) {
                    Swal.fire('Failed to load data. Error occured!', '', 'error');
                }
            });
        }

        function generateData(data, start_page) {
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    var tr = $('<tr/>');
                    var check = $('<td/>');
                    var subyek = $('<td/>');
                    var waktu = $('<td/>');
                    var lbs_lat = $('<td/>');
                    var lbs_lng = $('<td/>');
                    var wa_lat = $('<td/>');
                    var wa_lng = $('<td/>');
                    var tindakan = $('<td/>');
                    var action = $('<td/>');

                    action.css('width', '120px');
                    action.addClass('sticky-col');
                    check.css('width', '40px');

                    var icheck = $('<div/>');
                    icheck.addClass('icheck-list');
                    icheck.append('<label for="check-' + i + '"><input id="check-' + i + '" type="checkbox" class="select-check icheck select-multiple" data-ref="' + data[i]['id'] + '" /></label>');

                    check.append(icheck);

                    subyek.append(data[i]['subyek']);
                    waktu.append(data[i]['waktu']);
                    lbs_lat.append(data[i]['lbs_lat']);
                    lbs_lng.append(data[i]['lbs_lng']);
                    wa_lat.append(data[i]['wa_lat']);
                    wa_lng.append(data[i]['wa_lng']);
                    tindakan.append(data[i]['tindakan']);

                    var detail = $('<button/>');
                    detail.prop('type', 'button');
                    detail.addClass('btn btn-xs btn-default detail-data');
                    detail.append('<i class="fa fa-eye"></i>');
                    detail.attr('data-ref', data[i]['id']);
                    detail.attr('data-toggle', 'modal');
                    detail.attr('data-target', '#modalDetail');
                    detail.css('margin-right', '10px');

                    var remove = $('<button/>');
                    remove.prop('type', 'button');
                    remove.addClass('btn btn-xs btn-default remove-data');
                    remove.append('<i class="fa fa-trash"></i>');
                    remove.attr('data-ref', data[i]['id']);

                    action.append(detail);
                    action.append(remove);

                    tr.append(check);
                    tr.append(subyek);
                    tr.append(waktu);
                    tr.append(lbs_lat);
                    tr.append(lbs_lng);
                    tr.append(wa_lat);
                    tr.append(wa_lng);
                    tr.append(tindakan);
                    tr.append(action);

                    $('#table-order tbody').append(tr);

                    start_page++;
                }
            }

            $('.select-check').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
            });
        }

        function pagination(options, start_page, paginate) {
            var start_page_counter = (start_page - 1) + paginate;
            var pagination = $('#pagination');

            var ul = $('<ul/>');
            ul.addClass('pagination pagination-sm m-0 float-right');

            if (options['current_page'] > 10) {
                var div = Math.floor(options['current_page'] / 10);
                var mod = options['current_page'] % 10;
                var prev_data = options['current_page'] - mod;

                var fp = 0;
                var lp = 0;

                if (mod == 0) {
                    fp = ((div - 1) * 10) + 1;
                    lp = options['current_page'];
                } else {
                    fp = (div * 10) + 1;
                    lp = (div * 10) + 10;
                }

                var t = 0;
                for (var r = fp; r <= lp; r++) {
                    pages[t] = r;

                    t++;
                }
            } else if (options['current_page'] > 0 && options['current_page'] <= 10) {
                pages = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            }

            ul.append('<li class="page-item"><a href="javascript:;" data-page="1" class="paginate page-link">First</a></li>');

            if (options['prev_page_url'] != null) {
                var prev_page = options['current_page'] - 1;

                ul.append('<li class="page-item"><a href="javascript:;" data-page="' + prev_page + '" class="paginate page-link"><i class="fa fa-angle-left"></i></a></li>');
            }

            var b = 0;
            for (var j = 0; j < pages.length; j++) {
                if (pages[j] > options['last_page']) {
                    break;
                }

                var active_class = '';
                var styles = '';

                if (options['current_page'] == pages[j]) {
                    active_class = 'active';
                    styles = ''
                }

                ul.append('<li class="page-item ' + active_class + '"><a href="javascript:;" data-page="' + pages[j] + '" class="paginate page-link" style="' + styles + '">' + pages[j] + '</a></li>');

                b++;
            }

            if (options['current_page'] < options['last_page']) {
                var next_page = options['current_page'] + 1;

                ul.append('<li class="page-item"><a href="javascript:;" data-page="' + next_page + '" class="paginate page-link"><i class="fa fa-angle-right"></i></a></li>');
            }

            ul.append('<li class="page-item"><a href="javascript:;" data-page="' + options['last_page'] + '" class="paginate page-link">Last</a></li>');

            pagination.append(ul);

            $('#page-info').html('Showing ' + start_page + ' to ' + start_page_counter + ' of ' + options['total'] + ' data');
        }

        getData();
    });
</script>
@endsection