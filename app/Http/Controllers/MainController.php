<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\MainImport;
use App\Models\Subject;
use App\Models\Trace;

class MainController extends Controller
{

    public function index() {
        return view('content.main.index');
    }

    public function import(Request $request)
    {
        $response['status'] = false;
        $response['message'] = '';
        $isImported = false;
        $data = [];

        $file = $request->file('file');
	    $delimiter = $this->detectDelimiter($file);

        try {
            $data = Excel::toArray(new MainImport($delimiter), $file)[0];
            $response['data'] = $data;
            $isImported = true;
        } catch(\Exception $e) {
            $response['message'] = $e->getMessage();
        }

        if($isImported) {
            if(count($data) > 0) {
                foreach($data as $row) {
                    $id = $this->getSubjectId($row['subyek']);
                    $create = $this->createTracing($id, $row);
                }

                $response['status'] = true;
            }
        }

        return response()->json($response, 200);
    }

    public function data(Request $request) 
    {
        $page = $request->input('page');
        $paginate = intval($request->input('paginate'));

        $response['start_page'] = (($page-1)*$paginate) + 1;
        $response['paginate'] = $paginate;
        $response['data'] = [];

        $data = Trace::select('subjects.*', 'traces.*', 'subjects.id as subjectId')->join('subjects', 'subjects.id', '=', 'traces.id_subyek')
        ->whereNull('traces.deleted_at')
        ->whereNull('subjects.deleted_at')
        ->paginate($paginate);

        foreach($data as $row) {
            $rowdata['id'] = $row->subjectId;
            $rowdata['subyek'] = $row->subyek ;
            $rowdata['waktu'] = $row->waktu;
            $rowdata['lbs_lat'] = $row->lbs_lat;
            $rowdata['lbs_lng'] = $row->lbs_lng;
            $rowdata['wa_lat'] = $row->wa_lat;
            $rowdata['wa_lng'] = $row->wa_lng;
            $rowdata['tindakan'] = $row->tindakan;

            array_push($response['data'], $rowdata);
        }

        $response['options'] = $data;

        return response()->json($response, 200);
    }

    public function detail(Request $request) {
        $id = $request->input('id');

        $data = Trace::select('subjects.*', 'traces.*', 'subjects.id as subjectId')->join('subjects', 'subjects.id', '=', 'traces.id_subyek')
        ->whereNull('traces.deleted_at')
        ->whereNull('subjects.deleted_at')
        ->where('traces.id', $id)
        ->first();

        return response()->json($data, 200);
    }

    private function createTracing($subjek, $trace) 
    {
        $data = new Trace();
        $data->id_subyek = $subjek;
        $data->waktu = $trace['datetime'];
        $data->lbs_lat = $trace['lbs_lat'];
        $data->lbs_lng = $trace['lbs_lng'];
        $data->wa_lat = $trace['warning_area_lat'];
        $data->wa_lng = $trace['warning_area_lng'];
        $data->info_it = $trace['it'];
        $data->info_sv = $trace['sv'];
        $data->info_medsos = $trace['medsos'];
        $data->info_dana = $trace['pendanaan'];
        $data->info_agen_handling = $trace['agen_handling'];
        $data->analisa_intel_tek = $trace['intel_tek'];
        $data->analisa_niat = $trace['niat'];
        $data->analisa_motivasi = $trace['motivasi'];
        $data->analisa_kelompok = $trace['kelompok'];
        $data->analisa_akses = $trace['akses'];
        $data->analisa_dana = $trace['dana'];
        $data->ancaman_low = $trace['low'];
        $data->ancaman_medium = $trace['medium'];
        $data->ancaman_high = $trace['high'];
        $data->ancaman_middle = $trace['middle'];
        $data->ancaman_netral = $trace['netral'];
        $data->tindakan = $trace['tindakan'];
        $data->save();

        return true;
    }

    private function getSubjectId($subject) 
    {
        $id = null;

        $countSubject = Subject::whereNull('deleted_at')->whereRaw('LOWER(subyek) = "' . strtolower($subject) . '"');

        if($countSubject->count() == 0) {
            $data = new Subject();
            $data->subyek = $subject;

            if($data->save()) {
                $id = $data->id;
            }
        } else {
            $id = $countSubject->first()->id;
        }

        return $id;
    }

    private function detectDelimiter($file)
    {
        $delimiters = [';' => 0, ',' => 0, "\t" => 0, '|' => 0];

        $handle = fopen($file, "r");
        $firstLine = fgets($handle);
        fclose($handle);
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($firstLine, $delimiter));
        }

        return array_search(max($delimiters), $delimiters);
    }

}