<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class MainImport implements WithHeadingRow, WithCustomCsvSettings {

    private $delimiter;

    public function __construct($delimiter){
        $this->delimiter = $delimiter;
    }

	public function headingRow(): int {
        return 1;
    }

    public function getCsvSettings(): array {
    	return [
    		'delimiter' => $this->delimiter
    	];
    }

}