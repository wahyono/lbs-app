<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Subject;

class Trace extends Model
{
    protected $table = 'traces';
    protected $primaryKey = 'id';

    const UPDATED_AT = 'updated_at';
    const DELETED_AT = 'deleted_at';

    public function subject() {
        return $this->hasOne(Subject::class);
    }
}
