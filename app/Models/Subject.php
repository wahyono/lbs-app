<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Trace;

class Subject extends Model
{
    protected $table = 'subjects';
    protected $primaryKey = 'id';

    const UPDATED_AT = 'updated_at';
    const DELETED_AT = 'deleted_at';

    public function trace() {
        return $this->hasMany(Trace::class);
    }
}
