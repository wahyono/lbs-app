<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTraces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traces', function (Blueprint $table) {
            $table->id()->unsigned(false)->index();
            $table->bigInteger('id_subyek')->index();
            $table->dateTime('waktu')->nullable()->default(null);
            $table->double('lbs_lat')->nullable()->default(null);
            $table->double('lbs_lng')->nullable()->default(null);
            $table->double('wa_lat')->nullable()->default(null);
            $table->double('wa_lng')->nullable()->default(null);
            $table->tinyInteger('info_it')->nullable()->default(null);
            $table->tinyInteger('info_sv')->nullable()->default(null);
            $table->tinyInteger('info_medsos')->nullable()->default(null);
            $table->tinyInteger('info_dana')->nullable()->default(null);
            $table->tinyInteger('info_agen_handling')->nullable()->default(null);
            $table->tinyInteger('analisa_intel_tek')->nullable()->default(null);
            $table->tinyInteger('analisa_niat')->nullable()->default(null);
            $table->tinyInteger('analisa_motivasi')->nullable()->default(null);
            $table->tinyInteger('analisa_kelompok')->nullable()->default(null);
            $table->tinyInteger('analisa_akses')->nullable()->default(null);
            $table->tinyInteger('analisa_dana')->nullable()->default(null);
            $table->tinyInteger('ancaman_low')->nullable()->default(null);
            $table->tinyInteger('ancaman_medium')->nullable()->default(null);
            $table->tinyInteger('ancaman_high')->nullable()->default(null);
            $table->tinyInteger('ancaman_middle')->nullable()->default(null);
            $table->tinyInteger('ancaman_netral')->nullable()->default(null);
            $table->string('tindakan')->nullable()->default(null);
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->nullable()->default(null)->useCurrentOnUpdate();
            $table->dateTime('deleted_at')->nullable()->default(null);
            $table->foreign('id_subyek')->references('id')->on('subjects')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traces');
    }
}
